import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * Created by Srinivas on 01-02-2016.
 */
public class WhenAUserSearchesOnGoogle {

    private GoogleSearchPage page;

    @Before
    public void openTheBrowser() {
        page = PageFactory.initElements(new FirefoxDriver(), GoogleSearchPage.class);
        page.open("http://google.co.in/");
    }

    @After
    public void closeTheBrowser() {
        page.close();
    }

    @Test
    public void whenTheUserSearchesForSeleniumTheResultPageTitleShouldContainCats() {

        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        page.searchFor("selenium");
        assertThat(page.getTitle(), containsString("selenium") );
    }

}
